﻿using System;
using AppKit;
using Foundation;

namespace BrightnessAdjApp
{
    public partial class ViewController : NSViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Do any additional setup after loading the view.
        }

        partial void IncreaseBrightness(NSObject sender)
        {

            //var attributesWindow = new WindowManagerLayoutParams();

            //attributesWindow.CopyFrom(Window.Attributes);

            //attributesWindow.ScreenBrightness = .2f;

            //Window.Attributes = attributesWindow;
        }

        //public void SetBrightness(float brightness)
        //{
        //    UIScreen.MainScreen.Brightness = brightness;
        //}

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }
    }
}
