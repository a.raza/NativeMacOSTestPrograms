﻿using System;

using AppKit;
using Foundation;

namespace MyNewMacApp
{
    public partial class ViewController : NSViewController
    {
        private int numberOfTimesClicked = 0;
        
        public ViewController(IntPtr handle) : base(handle)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            TheTextLabel.StringValue = "Button has not been clicked yet.";

            // Do any additional setup after loading the view.
        }

        partial void ClickMEButton(Foundation.NSObject sender)
        {
            //Close the Window
            //this.View.Window.Close();

            numberOfTimesClicked++;
            
            // Update counter and label
            TheTextLabel.StringValue = string.Format("The button has been clicked {0} time{1}.", numberOfTimesClicked, (numberOfTimesClicked < 2) ? "" : "s");
        }


        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }
    
        public override void ViewWillAppear()
        {
            base.ViewWillAppear();

            // Set Window Title
            this.View.Window.Title = "Manu";
        }
    }
}
