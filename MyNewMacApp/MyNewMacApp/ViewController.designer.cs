// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MyNewMacApp
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        AppKit.NSTextField TheTextLabel { get; set; }

        [Action ("ClickMEButton:")]
        partial void ClickMEButton (Foundation.NSObject sender);
        
        void ReleaseDesignerOutlets ()
        {
            if (TheTextLabel != null) {
                TheTextLabel.Dispose ();
                TheTextLabel = null;
            }
        }
    }
}
