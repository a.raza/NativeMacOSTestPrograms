﻿using System;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace NestSupport
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            TwilioClient.Init(
                Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID"),
                Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN"));

            MessageResource.Create(
                to: new PhoneNumber("+923112997962"),
                from: new PhoneNumber("+12406508876"),
                body: "Ahoy from Twilio!");
        }
    }
}
