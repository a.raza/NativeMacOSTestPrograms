﻿using System;
using System.Collections.Generic;

namespace tested
{
    class MainClass
    {
        public static void Main(string[] args)
        {

        }
    }

    class myFun<T> : Fun<T>
    {
        static void F()
        {
            myFun<T> dt = new myFun<T>();
            myFun<int> di = new myFun<int>();
            myFun<string> ds = new myFun<string>();
            dt.x = default(T);
            di.x = 76;
            ds.x = "fun";
        }
    }

    class Fun<T>
    {
        protected T x;
    }
}
